package uk.co.adilparvez.fenneciconmod;


import android.content.res.XModuleResources;

import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.callbacks.XC_InitPackageResources;

public class FennecMod {

    private static String MODULE_PATH;

    public static void initZygote(IXposedHookZygoteInit.StartupParam startupParam) throws Throwable {
        MODULE_PATH = startupParam.modulePath;
    }

    public static void handleInitPackageResources(XC_InitPackageResources.InitPackageResourcesParam initPackageResourcesParam) throws Throwable {
        if (!initPackageResourcesParam.packageName.startsWith("org.mozilla")) {
            return;
        }

        XModuleResources moduleResources = XModuleResources.createInstance(MODULE_PATH, initPackageResourcesParam.res);
        if (initPackageResourcesParam.packageName.equals("org.mozilla.firefox")) {
            initPackageResourcesParam.res.setReplacement("org.mozilla.firefox:drawable/icon", moduleResources.fwd(R.drawable.ic_launcher));
        }
        if (initPackageResourcesParam.packageName.equals("org.mozilla.fennec_fdroid")) {
            initPackageResourcesParam.res.setReplacement("org.mozilla.fennec_fdroid:drawable/icon", moduleResources.fwd(R.drawable.ic_launcher));
        }
    }

}
